from django.http import HttpResponse, HttpResponseNotAllowed
from django.template.loader import get_template
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect

from .models import User
from .forms import SignupForm


def signup_view(request):
    if request.method == "GET":
        context = {
            "title": "Sign Up",
            "bg": "om/vedtaegter.jpg",  # TODO: replace image
        }
        template = get_template("accounts/signup.html")
        return HttpResponse(template.render(context, request))

    if request.method != "POST":
        return HttpResponseNotAllowed(["GET", "POST"])

    form = SignupForm(request.POST)
    if not form.is_valid():
        raise BadRequest(form.errors)

    # TODO: prettier error here
    if not form.cleaned_data["repeat_password"] == form.cleaned_data["password"]:
        raise BadRequest("Password and repeat password must match.")

    user = User.objects.create_user(
        username=form.cleaned_data["username"],
        email=form.cleaned_data["email"],
        password=form.cleaned_data["password"],
        phone=form.cleaned_data["phone"],
    )
    login(request, user)

    # TODO: redirect to somewhere sensible
    return HttpResponse("OK")


def login_view(request):
    if request.method == "GET":
        context = {
            "title": "Log In",
            "bg": "om/vedtaegter.jpg",  # TODO: replace image
        }
        template = get_template("accounts/login.html")
        return HttpResponse(template.render(context, request))

    if request.method != "POST":
        return HttpResponseNotAllowed(["GET", "POST"])

    username = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # TODO Redirect to a success page.
        return redirect("/")
        # return HttpResponse("OK")
    else:
        # TODO Return an 'invalid login' error message.
        raise BadRequest("Invalid credentials")


def logout_view(request):
    logout(request)
    # TODO Redirect to a success page.
    return HttpResponse("OK")


def auth_status_view(request):
    if not request.user.is_authenticated:
        return HttpResponse("Not logged in.")
    return HttpResponse(f"Logged in as {request.user.username}")
