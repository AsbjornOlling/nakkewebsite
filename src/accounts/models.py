from django.db import models
from year.models import VolunteerArea
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    phone = models.CharField(max_length=16)
#    user_status = models.ForeignKey(UserStatus, on_delete=models.CASCADE)
    REQUIRED_FIELDS = ["phone", "email"]

class UserStatus(models.Model):
    name = models.CharField(max_length=50)
    area = models.ForeignKey(VolunteerArea, on_delete=models.CASCADE)

