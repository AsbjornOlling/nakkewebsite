#!/usr/bin/env python3

from django.urls import path

from .views import signup_view, login_view, logout_view, auth_status_view


urlpatterns = [
    path("signup/", signup_view, name="signup"),
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("auth_status/", auth_status_view, name="auth_status"),
]
