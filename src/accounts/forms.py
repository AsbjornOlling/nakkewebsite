#!/usr/bin/env python3

from django import forms
from year.models import VolunteerArea

class SignupForm(forms.Form):
    username = forms.CharField(label="Name", max_length=100)
    email = forms.CharField(label="Email", max_length=200)
    phone = forms.CharField(label="Phone Nimber", max_length=16)
    password = forms.CharField(label="Password", min_length=10)
    repeat_password = forms.CharField(label="Repeat Password", min_length=10)
    area = forms.ModelChoiceField(queryset=VolunteerArea.objects.all())
