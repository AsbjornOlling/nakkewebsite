# Temp file for global template variable, while working in dev env
# TODO
# Remove this file in prod

from django.utils import timezone
from datetime import timedelta, datetime
from .models import Year

def current_year(request):
    # Get the current year
    cur_year = timezone.now().year

    # Set the month to November
    november_date = datetime(cur_year, 11, 1)  # November 1st of the current year

    # Subtract 90 days to approximate November from the current date
    november_date -= timedelta(days=90)

    # Get or create the Year object
    year_obj, created = Year.objects.get_or_create(year=november_date.year)

    return {'cur_year': year_obj}
    # cur_year = Year.objects.get_or_create(year=timezone.now().year)[0] - timedelta(days=90)
    # return {'cur_year': cur_year}

def global_context(request):
    return {
            'STATIC_DEV': 'https://dev.nakkefestival.dk/static/',
    }
