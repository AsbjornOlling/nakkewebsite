from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField

class Year(models.Model):
    year = models.IntegerField()
    published = models.BooleanField(default=False)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    
    def current_year(cls):
        return cls.objects.get_or_create(year=timezone.now().year)[0]

    def __str__(self):
        return str(self.year)


class Page(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE)
    url = models.CharField(max_length=100, unique=True)
    
    published = models.BooleanField(default=False)
    title = models.CharField(max_length=50)
    bg_image = models.ImageField(upload_to='images/pages/', blank=True)
    short_description = models.CharField(max_length=500, blank=True)
    long_description = models.TextField(blank=True)
    
    def __str__(self):
        """
        How the record is represented in the django admin interface
        """
        return self.title


class Donation(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='donations')
    
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    receiver = models.CharField(max_length=50)
    published = models.BooleanField(default=False)
    phone = models.CharField(max_length=50, blank=True)
    mail = models.EmailField(blank=True)
    link = models.URLField(blank=True)

    def __str__(self):
        return f'{self.year} - {self.amount} kr.'
    
class Event(models.Model):
    name = models.CharField(max_length=50)
    # img = models.CharField(max_length=250, blank=True)
    short_description = models.CharField(max_length=250, blank=True)
    long_description = models.TextField(max_length=2500, blank=True)
    img = models.ImageField(upload_to='images/events/', blank=True)
    # state = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Artist(models.Model):
    name = models.CharField(max_length=100)
    short_description = models.CharField(max_length=250, blank=True)
    long_description = models.TextField(blank=True, max_length=2500)
    img = models.ImageField(upload_to='images/artists/', blank=True)
    # mp3 = models.CharField(max_length=50, blank=True)
    link_youtube = models.URLField(blank=True)
    link_spotify = models.URLField(blank=True)
    link_soundcloud = models.URLField(blank=True)
    link_bandcamp = models.URLField(blank=True)
    link_other = models.URLField(blank=True)
    # state = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Stage(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Program(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='programs')
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE, related_name='programs')
    artist_id = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='programs', blank=True, null=True)
    event_id = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='programs', blank=True, null=True)

    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True)
    published = models.BooleanField(default=False)
    
    def __str__(self):
        if self.artist_id: 
            return f'{self.year} - {self.artist_id}'
        else: 
            return f'{self.year} - {self.event_id}'

class ApplicationStatus(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class ArtistApplication(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='artist_application')
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='artist_application')
    time = models.DateTimeField()
    status = models.ForeignKey(ApplicationStatus, on_delete=models.CASCADE, related_name='artist_application')

    techrider = models.FileField(upload_to='techriders/')
    comment = models.TextField(blank=True)

class Venue(models.Model):
    name = models.CharField(max_length=50)
    adress = models.CharField(max_length=50, blank=True)
    phone = models.CharField(max_length=50, blank=True)
    link = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Concert(models.Model):
    time = models.DateTimeField()
    comment = models.TextField(max_length=250, blank=True)

    def __str__(self):
        return self.time

class Genre(models.Model):
    tag = models.CharField(max_length=50)

    def __str__(self):
        return self.tag

class ArtistManagement(models.Model):
    comment = models.TextField(max_length=250)
    votes = models.IntegerField()
    stars = models.IntegerField()
    commentCount = models.IntegerField()


class VolunteerApplication(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='volunteer_application')
    time = models.DateTimeField()
    area = models.ForeignKey("VolunteerArea", on_delete=models.CASCADE, related_name='volunteer_application')
    status = models.ForeignKey(ApplicationStatus, on_delete=models.CASCADE, related_name='volunteer_application')
    comment = models.TextField(blank=True)

    def __str__(self):
        return f'{self.area} - {self.time}'

class VolunteerArea(models.Model):
    name = models.CharField(max_length=50)
    published = models.BooleanField()
    mail = models.CharField(max_length=50)
    volunteerCount = models.IntegerField()
    volunteerMax = models.IntegerField()
    managerCount = models.IntegerField()
    managerMax = models.IntegerField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    bg = models.ImageField(upload_to='images/areas/', blank=True)
    short_description = models.CharField(max_length=250, blank=True)
    long_description = models.TextField(max_length=2500, blank=True)

    def __str__(self):
        return self.name
    
class TicketType(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='tickettype')
    name = models.CharField(max_length=50)
    price = models.CharField(max_length=50)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    description = models.CharField(max_length=500)
    stock = models.IntegerField()
    onlineSale = models.IntegerField()
    doorSale = models.IntegerField()
    published = models.BooleanField()

    def __str__(self):
        return f'{self.year} - {self.name}'
    
class Ticket(models.Model):
    ticketType = models.ForeignKey("TicketType", on_delete=models.CASCADE, related_name='ticket')
    purchase_date = models.DateField(max_length=50)

    def __str__(self):
        return f'{self.ticketType} - {self.purchase_date}'
