from django.views.generic import TemplateView, ListView, DetailView, UpdateView 
# from django.views.generic.edit import UpdateView
from django.shortcuts import redirect, get_object_or_404 
from django.urls import reverse
from . import urls 
from year.models import Year, Page, ArtistApplication, VolunteerApplication, Donation, Event, Artist, Program, VolunteerArea
from .forms import YearForm, PageForm, ArtistApplicationForm, VolunteerApplicationForm, DonationForm, EventForm, ArtistForm, ProgramForm, VolunteerAreaForm


class IndexView(TemplateView):
    template_name = "hzd/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['title'] = Page.objects.get(id=16).title
        context['bg'] = Page.objects.get(id=16).bg_image
        
        return context

class YearView(ListView):
    template_name = "hzd/year/index.html"
    context_object_name = 'years'
    
    def get_queryset(self):
        # Get the sorting parameter from the request
        sort_by = self.request.GET.get('sort', '-year')
        # Sort queryset based on the parameter
        return Year.objects.all().order_by(sort_by)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=33).title
        context['bg'] = Page.objects.get(id=33).bg_image
        return context

class YearEdit(UpdateView):
    template_name = "hzd/year/year.html"
    context_object_name = 'year_edit'
    model = Year
    form_class = YearForm
    success_url = '/hzd/aar/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=34).title
        context['bg'] = Page.objects.get(id=34).bg_image
        return context

class PageView(ListView):
    template_name = "hzd/sider/index.html"
    context_object_name = 'pages'
    
    def get_queryset(self):
        # Get the sorting parameter from the request
        sort_by = self.request.GET.get('sort', '-url')
        # Sort queryset based on the parameter
        return Page.objects.all().order_by(sort_by)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=19).title
        context['bg'] = Page.objects.get(id=19).bg_image
        return context

class PageEdit(UpdateView):
    template_name = "hzd/sider/side.html"
    context_object_name = 'page_edit'
    model = Page
    form_class = PageForm
    success_url = '/hzd/sider/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=20).title
        context['bg'] = Page.objects.get(id=20).bg_image
        return context

class BookingView(ListView):
    template_name = "hzd/booking/index.html"
    context_object_name = 'artist_applications'
    model = ArtistApplication

    def get_queryset(self):
        sort_by = self.request.GET.get('sort', '-time')
        return ArtistApplication.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=17).title
        context['bg'] = Page.objects.get(id=17).bg_image
        return context

class BookingEdit(UpdateView):
    template_name = "hzd/booking/booking.html"
    context_object_name = 'booking_edit'
    model = ArtistApplication
    form_class = ArtistApplicationForm
    success_url = '/hzd/booking/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=22).title
        context['bg'] = Page.objects.get(id=22).bg_image
        return context

class DonationView(ListView):
    template_name = "hzd/donations/index.html"
    context_object_name = 'donations'
    model = Donation
    
    def get_queryset(self):
        sort_by = self.request.GET.get('sort', '-year')
        return Donation.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=29).title
        context['bg'] = Page.objects.get(id=29).bg_image
        context['model'] = DonationForm()
        return context

class DonationEdit(UpdateView):
    template_name = "hzd/donations/donation.html"
    context_object_name = 'donation_edit'
    model = Donation
    form_class = DonationForm
    success_url = '/hzd/donationer/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=30).title
        context['bg'] = Page.objects.get(id=30).bg_image
        return context

class VolunteerView(ListView):
    template_name = "hzd/frivillige/index.html"
    context_object_name = 'volunteer_applications'
    
    def get_queryset(self):
        sort_by = self.request.GET.get('sort', '-time')
        return VolunteerApplication.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=18).title
        context['bg'] = Page.objects.get(id=18).bg_image
        return context

class VolunteerEdit(UpdateView):
    template_name = "hzd/frivillige/frivillig.html"
    context_object_name = 'volunteer_edit'
    model = VolunteerApplication
    form_class = VolunteerApplicationForm
    success_url = '/hzd/frivillige/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=21).title
        context['bg'] = Page.objects.get(id=21).bg_image
        return context

class EventView(ListView):
    template_name = "hzd/events/index.html"
    context_object_name = 'events'
    
    def get_queryset(self):
        sort_by = self.request.GET.get('sort', '-name')
        return Event.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=25).title
        context['bg'] = Page.objects.get(id=25).bg_image
        return context

class EventEdit(UpdateView):
    template_name = "hzd/events/event.html"
    context_object_name = 'event_edit'
    model = Event
    form_class = EventForm
    success_url = '/hzd/events/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=26).title
        context['bg'] = Page.objects.get(id=26).bg_image
        return context

class ArtistView(ListView):
    template_name = "hzd/artists/index.html"
    context_object_name = 'artists'
    
    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'name')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order
        return Artist.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=27).title
        context['bg'] = Page.objects.get(id=27).bg_image
        return context

class ArtistEdit(UpdateView):
    template_name = "hzd/artists/artist.html"
    context_object_name = 'artist_edit'
    model = Artist
    form_class = ArtistForm
    success_url = '/hzd/kunstnere/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=28).title
        context['bg'] = Page.objects.get(id=28).bg_image
        return context

class ProgramView(ListView):
    template_name = "hzd/program/index.html"
    context_object_name = 'program'
    
    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'start_time')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order
        return Program.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=31).title
        context['bg'] = Page.objects.get(id=31).bg_image
        context['current_sort'] = self.sort_by
        context['next_order'] = self.next_order
        return context

class ProgramEdit(UpdateView):
    template_name = "hzd/program/program.html"
    context_object_name = 'program_edit'
    model = Program
    form_class = ProgramForm
    success_url = '/hzd/program/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=32).title
        context['bg'] = Page.objects.get(id=32).bg_image
        return context

class VolunteerAreaView(ListView):
    template_name = "hzd/areas/index.html"
    context_object_name = 'volunteer_areas'
    
    def get_queryset(self):
        # Setting default sorting order
        sort_field = self.request.GET.get('sort', 'name')
        current_order = self.request.GET.get('order', 'desc')
        # Changing between asc and desc
        if current_order == 'asc':
            sort_by = sort_field
            next_order = 'desc'
        else:
            sort_by = f'-{sort_field}'
            next_order = 'asc'
        
        self.sort_by = sort_field
        self.next_order = next_order
        return VolunteerArea.objects.all().order_by(sort_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=23).title
        context['bg'] = Page.objects.get(id=23).bg_image
        return context

class VolunteerAreaEdit(UpdateView):
    template_name = "hzd/areas/area.html"
    context_object_name = 'event_edit'
    model = VolunteerArea
    form_class = VolunteerAreaForm
    success_url = '/hzd/areas/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = Page.objects.get(id=24).title
        context['bg'] = Page.objects.get(id=24).bg_image
        return context
