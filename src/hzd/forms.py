from django import forms
from year.models import Year, Page, Donation, Event, Artist, Program, ApplicationStatus, ArtistApplication, Venue, Concert, Genre, ArtistManagement, VolunteerApplication, VolunteerArea
from django.contrib.postgres.forms import SimpleArrayField

class YearForm(forms.ModelForm):
    start_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    end_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    class Meta:
        model = Year
        fields = '__all__'
 
    def __init__(self, *args, **kwargs):
        super(YearForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['published'].label = "Udgivet"
        self.fields['start_time'].label = "Festivalstart"
        self.fields['end_time'].label = "Festivalslut"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'

class PageForm(forms.ModelForm):
    url = forms.CharField(disabled=True)
    year = forms.ModelChoiceField(queryset=Year.objects.all(), disabled=True)

    class Meta:
        model = Page
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(PageForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['url'].label = "URL"
        self.fields['published'].label = "Udgivet"
        self.fields['title'].label = "Titel"
        self.fields['bg_image'].label = "Billede"
        self.fields['short_description'].label = "Manchet"
        self.fields['long_description'].label = "Beskrivelse"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class DonationForm(forms.ModelForm):
    class Meta:
        model = Donation
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(DonationForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['amount'].label = "Beløb"
        self.fields['receiver'].label = "Modtager"
        self.fields['phone'].label = "Telefon"
        self.fields['mail'].label = "Email"
        self.fields['link'].label = "Link"
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Titel"
        self.fields['img'].label = "Billede"
        self.fields['short_description'].label = "Manchet"
        self.fields['long_description'].label = "Beskrivelse"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class ArtistForm(forms.ModelForm):
    class Meta:
        model = Artist
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(ArtistForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Navn"
        self.fields['short_description'].label = "Manchet"
        self.fields['long_description'].label = "Beskrivelse"
        self.fields['img'].label = "Billede"
        self.fields['link_youtube'].label = "YouTube-link"
        self.fields['link_spotify'].label = "Spotify-link"
        self.fields['link_soundcloud'].label = "Soundcloud-link"
        self.fields['link_bandcamp'].label = "Bandcamp-link"
        self.fields['link_other'].label = "Andet link"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name not in {'state','link'}:
                field.disabled = True
 
class ProgramForm(forms.ModelForm):
    start_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    end_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    class Meta:
        model = Program
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(ProgramForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['stage'].label = "Scene"
        self.fields['artist_id'].label = "Kunstner"
        self.fields['event_id'].label = "Event"
        self.fields['start_time'].label = "Starttid"
        self.fields['end_time'].label = "Sluttid"
        self.fields['published'].label = "Udgivet"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'
 
class ArtistApplicationForm(forms.ModelForm):
    time = forms.DateTimeField(
            widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}),
            disabled=True
            )
    artist = forms.ModelChoiceField(
        queryset=Artist.objects.all(),
        disabled=True,
    )
    year = forms.ModelChoiceField(
        queryset=Year.objects.all(),
        disabled=True,
    )
    status = forms.ModelChoiceField(
        queryset=ApplicationStatus.objects.all(),
    )
    
    class Meta:
        model = ArtistApplication
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ArtistApplicationForm, self).__init__(*args, **kwargs)
        self.fields['artist'].label = "Kunstner"
        self.fields['year'].label = "År"
        self.fields['time'].label = "Ansøgningstid"
        self.fields['status'].label = "Status"
        self.fields['techrider'].label = "Techrider"
        self.fields['comment'].label = "Kommentar"
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            # if field_name not in ['year']:
                field.widget.attrs['class'] = 'form-control'

class VenueForm(forms.ModelForm):
    class Meta:
        model = Venue
        fields = '__all__'
 
class ConcertForm(forms.ModelForm):
    class Meta:
        model = Concert
        fields = '__all__'
 
class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = '__all__'
 
class ArtistManagementForm(forms.ModelForm):

    class Meta:
        model = ArtistManagement
        fields = '__all__'
 
class VolunteerApplicationForm(forms.ModelForm):
    year = forms.ModelChoiceField(
        queryset=Year.objects.all(),
        disabled=True
    )
    status = forms.ModelChoiceField(
        queryset=ApplicationStatus.objects.all(),
    )
    time = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}),
        disabled=True
    )
    
    class Meta:
        model = VolunteerApplication
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(VolunteerApplicationForm, self).__init__(*args, **kwargs)
        self.fields['year'].label = "År"
        self.fields['time'].label = "Ansøgningstid"
        self.fields['area'].label = "Frivilligområde"
        self.fields['status'].label = "Status"
        self.fields['comment'].label = "Kommentar"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            # if field_name not in ['year']:
                field.widget.attrs['class'] = 'form-control'
 
class VolunteerAreaForm(forms.ModelForm):
    volunteerCount = forms.IntegerField(
            disabled=True
            )
    managerCount = forms.IntegerField(
            disabled=True
            )
    start_time = forms.DateTimeField(
            widget=forms.DateTimeInput(attrs={'type': 'datetime-local'})
            )
    end_time = forms.DateTimeField(
            widget=forms.DateTimeInput(attrs={'type': 'datetime-local'})
            )

    class Meta:
        model = VolunteerArea
        fields = '__all__'
 
    def __init__(self, *args, **kwargs):
        super(VolunteerAreaForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Navn"
        self.fields['published'].label = "Udgivet"
        self.fields['mail'].label = "Email"
        self.fields['volunteerCount'].label = "Tilmeldte frivillige"
        self.fields['volunteerMax'].label = "Frivilligpladser"
        self.fields['managerCount'].label = "Tilmeldte afviklere"
        self.fields['managerMax'].label = "Afviklerpladser"
        self.fields['start_time'].label = "Starttid"
        self.fields['end_time'].label = "Sluttid"
        self.fields['bg'].label = "Billede"
        self.fields['short_description'].label = "Manchet"
        self.fields['long_description'].label = "Beskrivelse"
        
        # Apply the 'form-control' class to all fields
        for field_name, field in self.fields.items():
            if field_name not in ['published']:
                field.widget.attrs['class'] = 'form-control'
            else:
                field.widget.attrs['class'] = 'form-switch'

    
