from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('aar/', views.YearView.as_view(), name='years'),
    path('aar/<int:pk>/', views.YearEdit.as_view(), name='year'),
    path('booking/', views.BookingView.as_view(), name='bookings'),
    path('booking/<int:pk>/', views.BookingEdit.as_view(), name='booking'),
    path('frivillige/', views.VolunteerView.as_view(), name='frivillige'),
    path('frivillige/<int:pk>/', views.VolunteerEdit.as_view(), name='frivillig'),
    path('sider/', views.PageView.as_view(), name='sider'),
    path('sider/<int:pk>/', views.PageEdit.as_view(), name='side'),
    path('donationer/', views.DonationView.as_view(), name='donations'),
    path('donationer/<int:pk>/', views.DonationEdit.as_view(), name='donation'),
    path('events/', views.EventView.as_view(), name='events'),
    path('events/<int:pk>/', views.EventEdit.as_view(), name='event'),
    path('kunstnere/', views.ArtistView.as_view(), name='artists'),
    path('kunstnere/<int:pk>/', views.ArtistEdit.as_view(), name='artist'),
    path('program/', views.ProgramView.as_view(), name='programs'),
    path('program/<int:pk>/', views.ProgramEdit.as_view(), name='program'),
    path('areas/', views.VolunteerAreaView.as_view(), name='areas'),
    path('areas/<int:pk>/', views.VolunteerAreaEdit.as_view(), name='area'), 
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
