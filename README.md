To run this program download all the files

$ sudo make run

$ sudo make migrate

$ sudo make manage COMMAND="loaddata artist"

$ sudo make manage COMMAND="loaddata page"

$ sudo make manage COMMAND="loaddata volunteerarea"

$ sudo make manage COMMAND="loaddata donation"

$ sudo make manage COMMAND="createsuperuser"

- Firstname: a
- Phone: a
- Mail: a@a.dk
- Password: a
- Doesn't live up to passwd reqyriments: y

If you have any problems, wipe the database
$ sudo rm -rf volumes/

Start over

Dependencies:
A docker version where docker includes compose. Test:
$ docker compose
